import { Component, AfterViewInit, ElementRef, HostListener } from '@angular/core';
import { jsPlumb, jsPlumbInstance } from 'jsplumb';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  title = 'matching-test';
  jsPlumbInstance;
  showConnectionToggle = false;
  arrayData = [];
  classList: any;
  idleft: any;
  constructor(private elementRef: ElementRef) {
  }
  ngAfterViewInit() {
    this.jsPlumbInstance = jsPlumb.getInstance();
    const boxs1 = this.elementRef.nativeElement.querySelectorAll('.group1 .card');
    boxs1.forEach(element => {
      element.addEventListener('click', this.selectSrc.bind(this));
    });
    const boxs2 = this.elementRef.nativeElement.querySelectorAll('.group2 .card');
    boxs2.forEach(element => {
      element.addEventListener('click', this.selectDes.bind(this));
    });
  }
  selectSrc(event) {
    this.idleft = event.currentTarget.id;
  }
  selectDes(event) {
    const idRight = event.currentTarget.id;
    if (idRight) {
      this.connect(this.idleft, idRight);
    }
  }
  connect(src: any, des: any) {
    const aaaa: any[] = this.jsPlumbInstance.getConnections();
    console.log(this.jsPlumbInstance.getConnections());
    const bbbb = aaaa.filter((x, y) => {
      if (x.sourceId === src) {
        this.jsPlumbInstance.deleteConnection(aaaa[y]);
        return;
      }
    });
    const ccc = aaaa.map((x, y) => {
      if (x.targetId === des) {
        this.jsPlumbInstance.deleteConnection(aaaa[y]);
        return;
      }
    });

    this.jsPlumbInstance.connect({
      connector: ['Bezier', { curviness: 30 }],
      setDragAllowedWhenFull: true,
      source: src,
      target: des,
      anchor: ['Right', 'Left'],
      paintStyle: { stroke: '#1089ff', strokeWidth: 2 },
      endpoint: ['Dot', { radius: 3, hoverClass: 'myEndpointHover' }, { cssClass: 'myCssClass' }]
    });
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    const aaaa: any[] = this.jsPlumbInstance.getConnections();
    const bbbb: any[] = aaaa.map(x => {
      this.jsPlumbInstance.deleteConnection(aaaa[x]);
    });

    const cccc: any[] = aaaa.map(x => {
      setTimeout(() => {
        this.connect(x.sourceId, x.targetId);
      }, 3000);
    });
  }
}
